// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// saya senang belajar JAVASCRIPT

// Jawaban soal 1
var kataSatuKalimatSatu = pertama.substring(0, 4);
var kataTigaKalimatSatu = pertama.substring(12, 18);
var kataDuaKalimatDua = kedua.substring(8, 18).toUpperCase();
var kalimat = kataSatuKalimatSatu.concat(" ").concat(kataTigaKalimatSatu).concat(" ").concat(kedua.substring(0, 7).concat(" ").concat(kataDuaKalimatDua));
console.log("Output soal 1: " + kalimat);

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// Jawaban soal 2
var kataPertama = parseInt(kataPertama);
var kataKedua = parseInt(kataKedua);
var kataKetiga = parseInt(kataKetiga);
var kataKeempat = parseInt(kataKeempat);

var hasil = kataPertama + kataKeempat + kataKedua * kataKetiga;

console.log("Output soal 2: " + hasil);

// Soal 3
var kalimat = "wah javascript itu keren sekali";

// Jawaban soal 3
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
