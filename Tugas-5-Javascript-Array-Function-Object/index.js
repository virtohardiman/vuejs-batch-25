// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// Jawaban soal 1
console.log("Jawaban soal 1: ");
var daftarHewanTerurut = daftarHewan.sort();
for (i = 0; i < daftarHewanTerurut.length; i++) {
  console.log(daftarHewanTerurut[i]);
}

// Jawaban soal 2
console.log("Jawaban soal 2: ");
function introduce(identity) {
  return "Nama saya " + identity.name + ", umur saya " + identity.age + " tahun, alamat saya di " + identity.address + ", dan saya punya hobby yaitu " + identity.hobby;
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Jawaban soal 3
console.log("Jawaban soal 3: ");
function hitung_huruf_vokal(kata) {
  var huruf = kata.toLowerCase().split("");
  var jumlahHuruf = 0;
  for (h = 0; h < huruf.length; h++) {
    if (huruf[h] == "a" || huruf[h] == "i" || huruf[h] == "u" || huruf[h] == "e" || huruf[h] == "o") {
      jumlahHuruf += 1;
    }
  }
  return jumlahHuruf;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

// Jawaban soal 4
console.log("Jawaban soal 4: ");
function hitung(integer) {
  var realInteger = integer - 2;
  var result = integer + realInteger;
  return result;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
