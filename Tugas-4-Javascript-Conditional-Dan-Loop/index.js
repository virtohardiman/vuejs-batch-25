// Soal 1
var nilai;

// Jawaban soal 1
console.log("Jawaban soal 1:");
nilai = 100;
if (nilai >= 85) {
  console.log("Indeks nilainya A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Indeks nilainya B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Indeks nilainya C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Indeks nilainya D");
} else {
  console.log("Indeks nilainya E");
}

// Soal 2
var tanggal = 8;
var bulan = 4;
var tahun = 2000;

// Jawaban soal 2
console.log("Jawaban soal 2:");
switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
    bulan = "Bulan tidak terdaftar";
}

console.log("Output tanggal lahir: " + tanggal + " " + bulan + " " + tahun);

// Jawaban soal 3
console.log("Jawaban soal 3:");

console.log("Output untuk n = 3:");
var hash = "";
for (n = 1; n < 4; n++) {
  hash += "#";
  console.log(hash);
}

console.log("Output untuk n = 7:");
var hash = "";
for (n = 1; n < 7; n++) {
  hash += "#";
  console.log(hash);
}

// Jawaban soal 4
console.log("Jawaban soal 4:");
var m = 10; //Contoh output 10
for (i = 1; i < m; i++) {
  if (i === 1 || i % 3 === 1) {
    console.log(i + " - I love programming");
  } else if (i === 2 || i % 3 === 2) {
    console.log(i + " - I love Javascript");
  } else if (i === 3 || i % 3 === 0) {
    console.log(i + " - I love VueJS");
    console.log("===");
  }
}
