var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Jawaban soal 2
var bookIndex = 0;
function remainingTime(time) {
  if (time > 0) {
    bookIndex += 1;
    readBooksPromise(time, books[bookIndex]);
  } else {
    return;
  }
}

readBooksPromise(10000, books[bookIndex]).then(remainingTime).catch(remainingTime);
