var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// Jawaban soal 1
var bookIndex = 0;
function remainingTime(time) {
  if (time > 0) {
    bookIndex += 1;
    readBooks(time, books[bookIndex], remainingTime);
  } else {
    return;
  }
}

readBooks(10000, books[bookIndex], remainingTime);
