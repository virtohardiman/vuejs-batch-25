// Jawaban soal 1
const kelilingPersegi = (panjang, lebar) => {
  let keliling = 2 * panjang + 2 * lebar;
  console.log(keliling);
};
const luasPersegi = (panjang, lebar) => {
  let luas = panjang * lebar;
  console.log(luas);
};

kelilingPersegi(4, 6);
luasPersegi(4, 6);

// Jawaban soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(`${firstName} ${lastName}`),
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

// Jawaban soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);

// Jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// Jawaban soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);
