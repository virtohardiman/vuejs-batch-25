// Jawaban soal 1
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

function jumlah_kata(kalimat) {
    var arrayKata = kalimat.trim().split(' ');
    var kata = arrayKata.length;
    console.log(kata);
}

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2

// Jawaban soal 2
function next_date(date, month, year) {
    var date1 = new Date();
    date1.setFullYear(year, (month - 1), (date + 1));  
    console.log(date1.toString().substring(4, 16));
}

// Contoh 1
var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

// Contoh 2
var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

// Contoh 3
var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021